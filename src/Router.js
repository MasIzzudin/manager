import React from 'react';
import { Router, Scene, Actions } from 'react-native-router-flux'; //router Navigation
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';
import EmployeeEdit from './components/EmployeeEdit';

const RouterComponent = () => {
    return (
        <Router sceneStyle={{ paddingTop: 55 }}>
        <Scene key="Auth">
            <Scene key='login' component={LoginForm} title="Kilogram" />
        </Scene>

        <Scene key='Main'>
            <Scene
            onRight={() => Actions.employeeForm()}
            rightTitle="Add" 
            key='employee' 
            component={EmployeeList} 
            title="Employee List" 
            initial
            />
            <Scene key='employeeForm' component={EmployeeCreate} title='Create a Employee' />
            <Scene key='employeeEdit' component={EmployeeEdit} title='Edit Employee' />
        </Scene>
        </Router>
    );
};

export default RouterComponent;
