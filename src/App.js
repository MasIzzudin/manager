import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxtThunk from 'redux-thunk';
import firebase from 'firebase';
import reducers from './reducers';
import Route from './Router';

export class App extends Component {
    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyBbrJmZkaeQdZc81Rp5_sr53nfC9gsd3_Y',
            authDomain: 'manager-1f86f.firebaseapp.com',
            databaseURL: 'https://manager-1f86f.firebaseio.com',
            projectId: 'manager-1f86f',
            storageBucket: 'manager-1f86f.appspot.com',
            messagingSenderId: '485833452849'
          };
          firebase.initializeApp(config);
    }

    render() {
        return (
            <Provider store={createStore(reducers, {}, applyMiddleware(ReduxtThunk))}>
                <Route />
           </Provider>
        );
    }
}
