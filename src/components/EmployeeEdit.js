import React, { Component } from 'react';
import _ from 'lodash';
import Communications from 'react-native-communications';
import { connect } from 'react-redux';
import { employeeUpdate, employeeSave, employeeDelete } from '../action';
import { Card, CardSection, Button, Popup } from './common';
import EmployeeForm from './EmployeeForm';

class EmployeeEdit extends Component {
    state = { showModal: false }
    componentWillMount() {
        _.each(this.props.employee, (value, prop) => {
            this.props.employeeUpdate({ prop, value });
        });
    }

    onButtonPress() {
        const { name, phone, shift } = this.props;

        this.props.employeeSave({ name, phone, shift, uid: this.props.employee.uid }); 
        //why props employee? because we in file the name EmployeeEdit.js, 
        //and if we want to click this button, 
        //we must go back into file ListItem.js where he throw employee as a props.
    }

    onMassagePress() {
        const { phone, shift } = this.props;

        Communications.text(phone, `Anda mendapatkan Shift Kerja pada hari ${shift}`);
    }

    onAccept() {
        const { uid } = this.props.employee;

        this.props.employeeDelete({ uid });
    }

    onDecline() {
        this.setState({ showModal: false });
    }

    render() {
        return ( 
            <Card>
                <EmployeeForm />
            
                <CardSection>
                    <Button onClick={this.onButtonPress.bind(this)}>
                        Save Changes
                    </Button>
                </CardSection>
                
                <CardSection>
                    <Button onClick={this.onMassagePress.bind(this)}>
                        Send a Massage
                    </Button>
                </CardSection>

                <CardSection>
                    <Button onClick={() => this.setState({ showModal: true })}>
                        Fire Employee
                    </Button>
                </CardSection>

                <Popup
                    visible={this.state.showModal}
                    onAccept={this.onAccept.bind(this)}
                    onDecline={this.onDecline.bind(this)}
                >
                    Are You sure you want to Delete this?!
                </Popup>
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    const { name, phone, shift } = state.employeeForm;

    return { name, phone, shift };
};

export default connect(mapStateToProps, { 
    employeeUpdate, employeeSave, employeeDelete })(EmployeeEdit);
