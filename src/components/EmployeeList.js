import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { ListView } from 'react-native';
import { employeeFetch } from '../action';
import { ListItem } from './ListItem';

class EmployeeList extends Component {
    componentWillMount() {
        this.props.employeeFetch();

        this.createDataSource(this.props);
    }

    componentWillReceiveProps(NextProps) {
        // nextProps are the Next set of props that this component 
        // will be rendered with
        // this.props is still the old set of props

        this.createDataSource(NextProps);
    }

    createDataSource({ employeeList }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(employeeList);
    }

    renderRow(employee) {
        return <ListItem employee={employee} />;
    }
    
    render() {
        return (
            <ListView
                enableEmptySections
                dataSource={this.dataSource}
                renderRow={this.renderRow}
            />
        );
    }
}

const mapStateToProps = state => {
    const employeeList = _.map(state.employeeList, (val, uid) => {
        return { val, uid }; // akan mengeluarkan array dari name, phone, dan shift
    });
    console.log(employeeList);
    return { employeeList };
};

export default connect(mapStateToProps, { employeeFetch })(EmployeeList);
