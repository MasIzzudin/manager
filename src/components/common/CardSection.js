import React from 'react';
import { View } from 'react-native';

export const CardSection = (props) => {
    return (
        <View style={style.CardSectionStyle}>
            {props.children}
        </View>
    );
};

const style = {
    CardSectionStyle: {
        borderBottomWidth: 1,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'relative',
        marginTop: 10,
        paddingLeft: 10
    }
};
