import React from 'react';
import { Text, View } from 'react-native';

export const Header = () => {
    const { TextStyle, ViewStyle } = style;

    return (
            <View style={ViewStyle}>
                <Text style={TextStyle}> Kilogram </Text>
            </View>
    );
};

const style = {
    ViewStyle: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        paddingTop: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 20 },
        shadowOpacity: 0.2,
        elevation: 10,
        position: 'relative'

    },

    TextStyle: {
        fontSize: 20,
        color: 'black' 
    }
};
