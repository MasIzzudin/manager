import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
import { CardSection } from './common';

export class ListItem extends Component {
    onSelectPress() {
        Actions.employeeEdit({ employee: this.props.employee });
    }
    render() {
        const { name } = this.props.employee;
        return (
            <TouchableWithoutFeedback
                onPress={this.onSelectPress.bind(this)}
            >
                <View>
                    <CardSection>
                        <Text style={Style.ListStyle}>
                            {name}
                        </Text>
                    </CardSection>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const Style = {
    ListStyle: {
        paddingLeft: 15,
        fontSize: 18
    }
};
