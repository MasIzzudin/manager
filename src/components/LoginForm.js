import React, { Component } from 'react';
import { Text } from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../action';
import { Card, CardSection, Input, Button, Spinner } from './common';

class LoginForm extends Component {
    onEmailChange(text) {
        this.props.emailChanged(text);
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text);
    }

    onPressButton() {
        const { email, password } = this.props;

        this.props.loginUser({ email, password });
    }

    handlePress() {
        if (this.props.loading) {
            return <Spinner size="large" />;
        }
        return (
            <Button onClick={this.onPressButton.bind(this)}>
                Login
            </Button>
            );
    }

    render() {
        return (
            <Card>
                <CardSection>
                    <Input 
                        label="Username"
                        placeholder="email@email.com"
                        onChangeText={this.onEmailChange.bind(this)}
                        value={this.props.email}
                    />
                </CardSection>

                <CardSection>
                    <Input
                        secureTextEntry 
                        label="Password"
                        placeholder="password"
                        onChangeText={this.onPasswordChange.bind(this)}
                        value={this.props.password}
                    />
                </CardSection>

                <Text style={Style.errorTextStyle}>
                    {this.props.error}
                </Text>

                <CardSection>
                   {this.handlePress()}
                </CardSection>
 
 
            </Card>
        );
    }
}

const Style = {
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    }
};

const mapStateToProps = ({ auth }) => {
    const { email, password, error, loading } = auth;

    return { email, password, error, loading };
};

export default connect(mapStateToProps, { emailChanged, passwordChanged, loginUser })(LoginForm);
