import React, { Component } from 'react';
import { View, Text, Picker } from 'react-native';
import { connect } from 'react-redux';
import { employeeUpdate } from '../action';
import { Input, CardSection } from './common';

class EmployeeForm extends Component {
    render() {
        return (
            <View>  
                <CardSection>
                    <Input 
                        label="Name"
                        placeholder="Si Fulan"
                        value={this.props.name}
                        onChangeText={value => this.props.employeeUpdate({ prop: 'name', value })}
                    />
                </CardSection>

                <CardSection>
                    <Input
                        label="Phone"
                        placeholder="08xx-xxxx-xxx"
                        value={this.props.phone}
                        onChangeText={value => this.props.employeeUpdate({ prop: 'phone', value })}
                    />
                </CardSection>

                <CardSection>
                    <Text style={Style.LabelStyle}>Shift</Text>
                    <Picker 
                    style={{ flex: 1 }}
                    selectedValue={this.props.shift}
                    onValueChange={value => this.props.employeeUpdate({ prop: 'shift', value })}
                    >
                        <Picker.Item label="Senin" value="Senin" />
                        <Picker.Item label="Selasa" value="Selasa" />
                        <Picker.Item label="Rabu" value="Rabu" />
                        <Picker.Item label="Kamis" value="Kamis" />
                        <Picker.Item label="Jum'at" value="Jum'at" />
                        <Picker.Item label="Sabtu" value="Sabtu" />
                        <Picker.Item label="Ahad" value="Ahad" />
                    </Picker>
                </CardSection>
            </View>
        );
    }
}

const Style = {
    LabelStyle: {
        fontSize: 18,
        marginTop: 10,
        paddingLeft: 20,
        flex: 1
    }
};

const mapStateToProps = (state) => {
    const { name, phone, shift } = state.employeeForm;

    return { name, phone, shift };
};

export default connect(mapStateToProps, { employeeUpdate })(EmployeeForm);

