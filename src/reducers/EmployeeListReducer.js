import { EMPLOYEES_FETCH_SUCCESS } from '../action/type';

const INITIAL_STATE = {}; //Main State untuk List

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case EMPLOYEES_FETCH_SUCCESS:
            console.log(action);
            return action.payload;
        default:
            return state;
    }
};
