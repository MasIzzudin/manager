import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import {
EMPLOYEE_UPDATE, // untuk merubah mengambil nilai state pada reducer, prop: name, prop: phone, dll
EMPLOYEE_CREATE, // mengembalikan nilai form pada default
EMPLOYEES_FETCH_SUCCESS, // memunculkan data pada database firebase ke ListView
EMPLOYEE_SAVE_SUCCESS, // mengembalikan nilai form pada default
EMPLOYEE_DELETE_SUCCESS // mengembalikan nilai form pada default
}
from './type';


export const employeeUpdate = ({ prop, value }) => {
    return {
        type: EMPLOYEE_UPDATE,
        payload: { prop, value }
    };
};

export const employeeCreate = ({ name, phone, shift }) => {
    const { currentUser } = firebase.auth();

return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/employees`)
        .push({ name, phone, shift })
        .then(() => {
            dispatch({ type: EMPLOYEE_CREATE });
            Actions.employee({ type: 'reset' });
        });    
    };
};

export const employeeFetch = () => {
    const { currentUser } = firebase.auth();

return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/employees`)
        .on('value', snapshot => {
        dispatch({ type: EMPLOYEES_FETCH_SUCCESS, payload: snapshot.val() });
        });
    };
};

export const employeeSave = ({ name, phone, shift, uid }) => {
    const { currentUser } = firebase.auth();

return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
        .set({ name, phone, shift })
        .then(() => {
            dispatch({ type: EMPLOYEE_SAVE_SUCCESS });
            Actions.employee({ type: 'reset' });
        });     
    };
};

export const employeeDelete = ({ uid }) => {
    const { currentUser } = firebase.auth();

return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
        .remove()
        .then(() => {
            dispatch({ type: EMPLOYEE_DELETE_SUCCESS });
            Actions.employee({ type: 'reset' });
        });     
    };
};
